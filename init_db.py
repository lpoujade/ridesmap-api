import sqlite3, time

conn = sqlite3.connect('instance/db_' + time.strftime('%M%M%S%S')  + '.sqlite')

# TODO parse from config (ini/yaml?)
conn.executescript("""
    create table tracks (
        id INTEGER PRIMARY KEY,
        title TEXT,
        desc TEXT,
        difficulty INTEGER,
        type INTEGER,
        xml TEXT,
        startposition TEXT);

    create table difficulty (
        id INTEGER PRIMARY KEY,
        str TEXT);

    create table type (
        id INTEGER PRIMARY KEY,
        str TEXT);

    create table comments (
        id INTEGER PRIMARY KEY,
        str TEXT);""")

conn.executemany('insert into type (str)  values (?)', [('cross',), ('muni',), ('road',)])
conn.executemany('insert into difficulty (str) values (?)', [('easy',), ('medium',), ('hard',)])
