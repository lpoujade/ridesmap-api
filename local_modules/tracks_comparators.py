def tc_id(val, track):
    return(val == track.id)

def tc_name(val, track):
    return(val == track.file)

track_comparators = {
    'id': tc_id,
    'name': tc_name
    }
