import os, sqlite3
from .tracks_comparators import *

print('[…] loading tracks')
db = 'instance/database.sqlite'

# -----------database-----------
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return(d)

def db_con(with_cursor=False):
    conn = sqlite3.connect(db, detect_types=sqlite3.PARSE_COLNAMES)
    conn.row_factory = dict_factory
    cur = conn.cursor()
    return( (conn, cur) if with_cursor else conn )
# ------------------------------

# -----------tracks-----------
def new(track):
    (conn, cur) = db_con(True)
    print('---- new track')
    # print(track)
    track_type = track['type'].lower()
    cur.execute('insert into tracks (title, type, difficulty, xml) values (?, ?, ?, ?)', (
        track['name'],
        track_type,
        track['difficulty'],
        track['string']
        ))
    lastrowid = cur.lastrowid
    conn.commit()
    conn.close()
    return(lastrowid)

def list():
    conn = db_con()
    tracks_list = []
    for row in conn.execute('SELECT * FROM tracks'):
        tracks_list.append(row)
    conn.close()
    return(tracks_list)

def find(arg):
    for track in tracks_list:
        for prop in arg.keys():
            if tracks_comparator[prop](arg[prop], track):
                return track
# ----------------------------


# -----------admin-----------
#def delete_track(track_id):
    #(conn, cur) = db_con(True)
    #print('---- delete track' + track_id)
    ## print(track)
    #cur.execute('delete from track where id = ?', track_id)
    #conn.commit()
    #conn.close()
    #return()
# -----------modo-----------
# def delete_com():
# def delete_pic():
