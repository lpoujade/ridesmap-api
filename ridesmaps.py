#!/usr/bin/python

# POST
# - addGPX      Object { title: text, gpx: bin/gpx? }
# - editTrack   Object { trackid, prop: value }

# GET
# - trackList   no params   array [ Object { trackname, props ... },  ... ]
# - track       trackid     Object { trackname, props, gpx datas }

import flask, json
from flask import jsonify, Flask, request
from local_modules import tracks

app = Flask(__name__)

@app.after_request
def set_header_cors(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = '*'
    return response

#@app.before_request
def handle_auth():
    print('PRE REQ HOOK')

@app.route('/tracks', methods=['GET'])
def get_tracks():
    resp = jsonify(tracks.list())
    print(resp)
    return(resp)

@app.route('/track/<tid>', methods=['GET'])
def get_track_by_id(track_id):
    return(tracks.find(track_id=track_id))

@app.route('/track/<track_id>', methods=['DELETE'])
def delete_track_by_id(track_id):
    return (track.delete(track_id=track_id))


@app.route('/track/<tid>/<tproperty>', methods=['GET'])
def get_track_property(tid, tproperty):
    track = tracks.find({'id': tid})
    return track[tproperty]

@app.route('/track', methods=['POST'])
def post_track():
    print("##########\n\nTODO check file size\n\n##########")
    track_id = tracks.new(json.loads(request.data))
    resp = jsonify({'track_id': track_id})
    return resp

@app.route('/track/<tid>/<tproperty>', methods=['POST'])
def post_track_property(tid, tproperty):
    track = tracks.find(tid)
    track[tproperty] = request.data
    track.save()
    return True

