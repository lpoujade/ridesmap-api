
## GET

### /tracks  
> json array with Track object, missing heavy properties
> (photos, videos …)

```
[
	{
		_id:	189273,
		title:	'the first track',
		desc:	'minimal description of track',
		gpx:	'<xml-gpx …>',
		len, positive_drop, negative_drop, difficulty,
		medias_count
	},
	{
		…
	},
	…
]
```


### /track/\<id\>  
> a single and complete Track object

```
	{
		_id:	189273,
		title:	'the first track',
		gpx:	'<xml-gpx …>',
		len, positive_drop, negative_drop, difficulty,
		medias: [
		{ _id, title, file, track_startpoint, track_endpoint, thumbnail },
		[…], […]
		]
	}
```

### /track/\<id\>/\<property\>  
> type depend of query, json object, file/url, html, text …



## POST

### /track
> add a new track

```
{
	name,
	difficulty,
	desc,
	gpx
}
```

### /track/\<id\>/\<properties\>
> edit a track

> type depend of property
