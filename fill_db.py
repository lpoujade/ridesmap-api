#!/usr/bin/python

import sys, os, random
from local_modules import tracks

location = sys.argv.pop(1)

if not location or not os.access(location, os.R_OK):
    print("MISSING/BAD GPX DIRECTORY")
    exit(3)

print('will load files from ' + location)

availables_types = ['muni', 'road', 'cross']
availables_level = ['easy', 'medium', 'hard', 'extreme']

gpx_files = os.listdir(location)
for gfile in gpx_files:
    print("Will add following GPX file: ", gfile)
    with open(location + gfile, 'r') as theFile:
        gdata = theFile.read()
    track_type = availables_types[random.randint(0, 2)]
    track_difficulty = availables_level[random.randint(1, 3)]
    tracks.new({'name': gfile, 'type': track_type, 'difficulty': track_difficulty, 'string': gdata})

